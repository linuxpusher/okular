# Translation of okular_chm to Norwegian Nynorsk
#
# Karl Ove Hufthammer <karl@huftis.org>, 2007, 2008, 2020.
# Eirik U. Birkeland <eirbir@gmail.com>, 2008.
msgid ""
msgstr ""
"Project-Id-Version: okular_chm\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-03-10 00:43+0000\n"
"PO-Revision-Date: 2020-03-15 19:04+0100\n"
"Last-Translator: Karl Ove Hufthammer <karl@huftis.org>\n"
"Language-Team: Norwegian Nynorsk <l10n-no@lister.huftis.org>\n"
"Language: nn\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 19.12.3\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Environment: kde\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Karl Ove Hufthammer"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "karl@huftis.org"

#: lib/ebook_epub.cpp:311
#, kde-format
msgid "Unsupported encoding"
msgstr "Ikkje-støtta teiknkoding"

#: lib/ebook_epub.cpp:311
#, kde-format
msgid ""
"The encoding of this ebook is not supported yet. Please open a bug in "
"https://bugs.kde.org for support to be added"
msgstr ""
"Teiknkodinga til denne e-boka er ikkje støtta enno. Skriv inn ein "
"feilrapport på https://bugs.kde.org/ om du ønskjer at støtte for "
"teiknkodinga skal leggjast til."
